select proName, depName, divName, proViewName, depViewName, divViewName, tblPurchReq.*
from 
(
Select * from 
(select tblPurchReq.PurchReqID, tblPurchReq.prqReqDesc, tblPart.GroupingID,
tblpurchreq.ProjectID,
tblpurchreq.DepartmentID,
tblpurchreq.DivisionID,
tblPurchReqPart.PartID,tblPurchReqPart.prpSeq,prqDate,invDate,
PurchReqCD,PartCD,PrtName,PrtViewName,prpQuantity as PR, gngName, GroupingCD, 
Isnull( (select sum(popQuantity) from tblpopart inner join tblPurchQuoPart on tblpopart.RefID=tblPurchQuoPart.PurchQuoID AND tblPOPart.RefSeqID=tblPurchQuoPart.SeqID where tblPurchQuoPart.RefSeqID=tblPurchReqPart.SeqID and POID in (Select POID from tblPO where POID=tblPOPart.POID and deleted=0)),0) as PO, 
Isnull( (select sum(InpQuantity) from tblInvPart Inner Join tblpopart On tblInvPart.RefID=tblpoPart.POID and tblInvPart.RefSeqID=tblPoPart.SeqID 
inner join tblPurchQuoPart on tblpopart.RefID=tblPurchQuoPart.PurchQuoID AND tblPOPart.RefSeqID=tblPurchQuoPart.SeqID 
where InventoryID = tblInventory.InventoryID and tblPurchQuoPart.RefSeqID=tblPurchReqPart.SeqID),0) as DO
from tblpurchreq 
inner join tblpurchreqpart on tblpurchreq.purchreqid=tblpurchreqpart.purchreqid 
left outer join tblPurchQuoPart on tblPurchQuoPart.RefID = tblPurchReqPart.PurchReqID and tblPurchQuoPart.RefSeqID = tblPurchReqPart.SeqID
left outer join tblPoPart on tblPurchQuoPart.SeqID = tblPoPart.RefSeqID 
and tblPurchQuoPart.PurchQuoID = tblPoPart.RefID 
left outer join tblInvPart on tblPoPart.POID = tblInvPart.RefID and tblPoPart.SeqID = tblInvPart.RefSeqID
inner join tblInventory on tblInvPart.InventoryID = tblInventory.InventoryID
inner join tblPart on tblPurchReqPart.PartID=tblPart.PartID 
left outer join tblGrouping ON tblPart.GroupingID = tblGrouping.GroupingID 
Where tblPurchReq.StatusID=2 ) a 
where (PR<>PO or PO<>DO or PR<>DO))tblPurchReq 
left outer join tblProject ON tblPurchReq.ProjectID = tblProject.ProjectID 
left outer join tblDepartment ON tblPurchReq.DepartmentID = tblDepartment.DepartmentID 
left outer join tblDivision ON tblPurchReq.DivisionID = tblDivision.DivisionID
